using System;
using System.Collections.Generic;
using System.Threading.Tasks;



namespace Instances
{
  public static class Instances
  {
    private static Object s_lockObj = new Object();


    private static Dictionary<Type, Object> s_instances;



    public static T GetInstance<T>()
      where T : class, new()
    {
      lock (Instances.s_lockObj) {

        if (Instances.s_instances == null) {
          Instances.s_instances = new Dictionary<Type, Object>();
        }

        Type type = typeof(T);

        if (!Instances.s_instances.ContainsKey(type)) {
          Instances.s_instances.Add(type, new T());
        }

        return (T)Instances.s_instances[type];

      }
    }


    public static void ResetInstance(Type instanceType, bool disposeInstance = true)
    {
      lock (Instances.s_lockObj) {

        if (Instances.s_instances != null && Instances.s_instances.ContainsKey(instanceType)) {
          if (disposeInstance) {
            Instances.TryDisposeOfInstance(Instances.s_instances[instanceType]);
          }
          Instances.s_instances.Remove(instanceType);
        }

      }
    }


    public static void ResetAllInstances(bool disposeInstances = true)
    {
      lock (Instances.s_lockObj) {

        if (Instances.s_instances != null) {
          if (disposeInstances) {
            foreach (Object instance in Instances.s_instances.Values) {
              Instances.TryDisposeOfInstance(instance);
            }
          }

          Instances.s_instances.Clear();

          Instances.s_instances = null;
        }

      }
    }


    private static void TryDisposeOfInstance(Object instance)
    {
      if (instance is IDisposable) {
        ((IDisposable)instance).Dispose();
      }
    }
  }
}

