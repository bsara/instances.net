Instances.NET Documentation
===========================

Table of Contents
-----------------

.. toctree::
   :maxdepth: 2

   intro
   namespace
   class-instances
   changelog
   license

