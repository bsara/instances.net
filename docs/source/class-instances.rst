***************
Instances Class
***************

.. code:: c#

  public static class Instances


A static class which acts as a `singleton <https://en.wikipedia.org/wiki/Singleton_pattern>`_
factory/manager.


.. note:: All methods in this class are `thread safe <https://en.wikipedia.org/wiki/Thread_safety>`_!



-----------


GetInstance()
=============

.. code-block:: c#

  public static T GetInstance<T>()


*TODO:* Add description


**Parameters**
  **T** - TODO


-----------


ResetAllInstances()
===================

.. code-block:: c#

 public static void ResetAllInstances(bool disposeInstances = false)



*TODO:* Add description


**Parameters**
  **disposeInstances** - TODO


-----------


ResetInstance()
=============

.. code-block:: c#

  public static void ResetInstance(System.Type instanceType, bool disposeInstance = false)


*TODO:* Add description


**Parameters**
  **instanceType** - TODO
  **disposeInstance** - TODO




.. |nbsp| unicode:: 0xA0
   :trim:
