*********
Namespace
*********

All classes use the same namespace: ``Instances``.

So, accessing this code in a normal situation would
cause you to have a ``using`` statement at the top
of your C# code like the following:

.. code-block:: c#

  using Instances;

  // ...
